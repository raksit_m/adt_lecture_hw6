import java.util.Arrays;


public class Quicksort {
	
	public static int partition(int[] array, int left, int right) {
		
		int pivot = array[left];
		
		int i = left;
		
		for(int j = left + 1; j <= right; j++) {
			
			if(array[j] <= pivot) {
				
				i++;
				
				int temp = array[i];
				
				array[i] = array[j];
				
				array[j] = temp;
				
				System.out.println(Arrays.toString(array));
				
			}
		}
		
		int temp = array[left];
		
		array[left] = array[i];
		
		array[i] = temp;
		
		System.out.println(Arrays.toString(array));
		
		return i;
	}
	
	public static void quickSort(int[] array, int left, int right) {
			
		if(left < right) {
			
			int index = partition(array, left, right);
			
			quickSort(array, left, index - 1);
			
			quickSort(array, index + 1, right);
			
		}		
	}
	
	public static void main(String[] args) {
		
		int[] array = {3, 1, 8, 2, 6, 7, 5, 10, 4};
		
		quickSort(array, 0, array.length - 1);
		
//		System.out.println(Arrays.toString(array));
		
	}
	
}
