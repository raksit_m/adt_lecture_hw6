
public class SplayTree<T extends Comparable<T>> {

	private TreeCell<T> root;
	
	private TreeCell<T> header;

	public SplayTree() {

		root = null;

	}

	public void insert(T datum) {

		TreeCell<T> cell;

		int c;

		if(root == null) {

			root = new TreeCell<T>(datum);

			return;
		}

		splay(datum);

		if((c = datum.compareTo(root.getDatum())) == 0) {

			return;
		}

		cell = new TreeCell<T>(datum);

		if (c < 0) {

			cell.setLeft(root.getLeft());

			cell.setRight(root);

			root.setLeft(null);

		} 

		else {

			cell.setRight(root.getRight());

			cell.setLeft(root);

			root.setRight(null);

		}

		root = cell;
	}

	private void splay(T datum) {

		TreeCell<T> l, r, t, y;

		l = r = header;
		
		t = root;

		for (;;) {

			if (datum.compareTo(t.getDatum()) < 0) {

				if (t.getLeft() == null) break;

				if (datum.compareTo(t.getLeft().getDatum()) < 0) {

					y = t.getLeft();                            /* rotate right */

					t.setLeft(y.getRight());

					y.setRight(t);

					t = y;

					if (t.getLeft() == null) break;

				}

				r.setLeft(t);                                 /* link right */

				r = t;

				t = t.getLeft();

			} 

			else if (datum.compareTo(t.getDatum()) > 0) {
				
				if (t.getRight() == null) break;

				if (datum.compareTo(t.getRight().getDatum()) < 0) {

					y = t.getRight();                            /* rotate left */

					t.setRight(y.getLeft());

					y.setLeft(t);

					t = y;

					if (t.getRight() == null) break;

				}

				l.setLeft(t);                                 /* link left */

				l = t;

				t = t.getRight();

			} else {
				
				break;
			}
		}
		
		l.setRight(t.getLeft());                                   /* assemble */
		
		r.setLeft(t.getRight());
		
		t.setLeft(header.getRight());
		
		t.setRight(header.getLeft());
		
		root = t;
	}
}
